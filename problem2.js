// Using callbacks and the fs module's asynchronous functions, do the following:

// 1. Read the given file lipsum.txt

const fs = require("fs");
const path = require("path");

async function readWordFiles() {
  try {
    pathfile = path.join(__dirname, "/lipsum.txt");
    let data = await fs.readFileSync(pathfile, "utf-8"); // reading file
    console.log("Read File Successfully , ");
    console.log("---------------------Data Below-----------------------\n");
    console.log(data);
    // console.log(data.split("\n"))
    console.log("\n\n");
  } catch (err) {
    console.log(
      "function (readWordFiles) Error occured in creating the file ",
      err
    );
    console.log("\n\n");
  }
}

// readWordFiles();

// 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt

async function newFileUpperCase() {
  try {
    pathfile = path.join(__dirname, "/lipsum.txt");
    let data = await fs.readFileSync(pathfile, "utf-8"); // reading file
    //   let words = data.toUpperCase().split(/\s+/);  // it is a regex function. that uses /\s+/, where one or more consecutive places. (WE can also do by regex)
    let words = data.split("\n"); // data is in th e format of 2-3 paragragh so splitting it. then , it will becomes - a Array
    words = words.map((index) => {
      // will iterate and sort() it
      index = index.split(" ");
      index = index.sort().join(" ");
      // console.log(index)
      return index;
    });
    words = words.join("\n");
    console.log(words);

    try {
      let writingNewFile1 = path.join(__dirname, "/filenames.txt");
      await fs.writeFileSync(writingNewFile1, words, "utf-8");
    } catch (error) {
      console.log(
        "function (newFileUpperCase) :- Error occured in creating a new file ",
        err
      );
    }

    //   console.log(words.sort().join(' '))
  } catch (err) {
    console.log(
      "function (newFileUpperCase) Error occured in reading the file ",
      err
    );
    console.log("\n\n");
  }
}

// newFileUpperCase()

// 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt

async function newFileLowerCase() {
  try {
    let pathfile = path.join(__dirname, "/lipsum.txt");
    let data = await fs.readFileSync(pathfile, "utf-8");
    let dataInLowerCase = data.toLowerCase();
    try {
      let writingNewFile2 = path.join(__dirname, "/filenames.txt");
      console.log(
        "function :- newFileLowerCase() - - - writing a lowerCase() to a newfile named:-  'filenames.txt' \n"
      );
      await fs.writeFileSync(writingNewFile2, dataInLowerCase, "utf-8");
      console.log("'filenames.txt' Successfully created\n");
    } catch (err) {
      console.log(
        "function(newFileLowerCase) Unfortunatley error occured in creating file"
      );
    }
  } catch (err) {
    console.log("function(newFileLowerCase) Unfortunatley error occured ", err);
  }
}
// newFileLowerCase()

// 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt

async function readNewFile() {
  try {
    const filepath = path.join(__dirname, "/filenames.txt");
    let data = fs.readFileSync(filepath, "utf-8");
    // console.log(data);
    let words = data.split("\n");
    words = words.map((index) => {
      index = index.toLowerCase().split(" ");
      index = index.sort().join(" ");
      //   console.log(index)
      return index;
    });
    words = words.join("\n");
    console.log(words);

    try {
      let writingNewFile2 = path.join(__dirname, "/filenames.txt");
      console.log(
        "function :- readNewFile() - - - writing a sorted content to a newfile named:-  'filenames.txt' \n"
      );
      await fs.writeFileSync(writingNewFile2, words, "utf-8");
      console.log("'filenames.txt' Successfully created\n");
    } catch (error) {
      console.log(
        "function(readNewFile) - Unfortunatley, error occured in creating file ",
        error
      );
    }
  } catch (err) {
    console.log("function(readNewFile) - Unfortunatley, error occured ", err);
  }
}
// readNewFile();

// 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

async function deleteFiles() {
  try {
    const filepath = path.join(__dirname, "/filenames.txt");
    fs.unlinkSync(filepath);
  } catch (error) {
    console.log("function(deleteFiles) - Unfortunatley, error occured ", error);
  }
}

// deleteFiles()

module.exports = {
  readWordFiles,
  newFileUpperCase,
  newFileLowerCase,
  readNewFile,
  deleteFiles,
};
