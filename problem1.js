const fs = require("fs").promises;
const path = require("path");

async function makeAndDelete() {
  try {
    let newDirectory = "goingToDelete";

    console.log("Making New Directory in Process . . . ");
    await fs.mkdir(newDirectory); // new directory
    console.log("Congratulations - New Directory is successfully Created");

    try {
      console.log("\nFiles are going to be created - - - ");
      for (let index = 0; index < 5; index++) {
        // generating a nnew 5 files
        let filepath = path.join(
          __dirname,
          `/${newDirectory}/`,
          `RandomFile${index}.json` // file path created
        );
        let content = `this file name is RandomFile${index}`;

        await fs.writeFile(filepath, content, "utf-8"); // for every iteration, filepath is assigned different name -and filepath is createDecipheriv.
      }
      console.log("files created");
    } catch (err) {
      console.log("Unfortunately, Error occured in making files- ", err);
    }
  } catch (err) {
    console.log("Unfortunately, Error occured in making Directory- ", err);
  }
}

makeAndDelete();

async function deleteFunc() {
  try {
    async function delay(n) {
      return new Promise((resolve) => {
        setTimeout(() => {
          //   resolve();
          resolve();
        }, n);
      });
    }

    await delay(10000);
    // Here i have given a time, to see the files, to be viewed.
    // after 10 seconds, the files are going to be deleted.
    console.log("\nFiles started getting deleted . . . ");

    for (let index = 0; index < 5; index++) {
      // iterating and deleting.
      let filepath = path.join(
        __dirname,
        `./goingToDelete/RandomFile${index}.json`
      );
      console.log(filepath);
      await fs.unlink(filepath); // fs.unlink is used to delete the file.
    }
    console.log("Files Deleted");

    console.log("Directory is going to be deleted . . . . ");

    let directoryPath = path.join(__dirname, "./goingToDelete");
    await fs
      .rmdir(directoryPath, { recursive: true }) // removing the directory - rmdir is useful to remove the directory!
      .then(() => {
        console.log("Directory removed Susseccfully");
      })
      .catch((err) => {
        console.error("Unfortunately, Eror occured in deleting the directory.");
      });
  } catch (err) {
    console.log("error happend", err);
  }
}
deleteFunc();
